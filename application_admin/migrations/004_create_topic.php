<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_topic extends CI_Migration {
  public function up(){
    $this->create_topic_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_topic');
  }
  private function create_topic_table(){
    $this->dbforge->add_field(array(
      'topic_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'topic_type' => array(
        'type' => 'ENUM',
        'constraint' => array('ask', 'recommend'),
        'default' => 'ask'
      ),
      'from_location' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'to_location' => array(
        'type' => 'VARCHAR',
        'constraint' => '255',
      ),
      'topic_detail' => array(
        'type' => 'TEXT'
      ),
      'user_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show'
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'last_login_date' => array(
        'type' => 'DATETIME',
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_by_type' => array(
        'type' => 'ENUM',
        'constraint' => array('admin', 'user'),
        'default' => 'admin'
      )
    ));
    $this->dbforge->add_key('topic_id', TRUE);
    $this->dbforge->add_key(array('user_id','enable_status','is_delete'));
    $this->dbforge->create_table('tbl_topic');
  }
}
