var BASE_URL = '';
var marker;
var initialLocation;
var defaultLad = 13.7122499;
var defaultLon = 100.59655819999999;
$(document).ready(function(){
  BASE_URL = $('#base_url').val();
  setOnChangeAssetType();
  setOnChangeProvince();
  setOnChangeAmphur();
  initialize();
  onChangeAssetType();
});
function initialize() {
  var mapOptions = {
    zoom: 15
  };
  var map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);
  // Try W3C Geolocation (Preferred)
  if(navigator.geolocation) {
    browserSupportFlag = true;
    console.log('support');
    navigator.geolocation.getCurrentPosition(function(position) {
      initialLocation = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
      map.setCenter(initialLocation);
      document.getElementById("txtLad").value = position.coords.latitude;
      document.getElementById("txtLon").value = position.coords.longitude;
      marker = new google.maps.Marker({
        position: initialLocation,
        map: map,
        draggable:true,
        title:"Kiosk Location!"
      });
      google.maps.event.addListener(marker, 'dragend', function(evt){
        document.getElementById("txtLad").value = evt.latLng.lat();
        document.getElementById("txtLon").value = evt.latLng.lng();
      });
    }, function() {
      handleNoGeolocation(browserSupportFlag);
    });
  }else {
    browserSupportFlag = false;
    handleNoGeolocation(browserSupportFlag);
  }
  google.maps.event.addListener(map, "rightclick", function(event) {
    var lat = event.latLng.lat();
    var lng = event.latLng.lng();
    document.getElementById("txtLad").value = lat;
    document.getElementById("txtLon").value = lng;
    var latlng = new google.maps.LatLng(lat, lng);
    marker.setPosition(latlng);
  });

  function handleNoGeolocation(errorFlag) {
    initialLocation = new google.maps.LatLng(13.7122499,100.59655819999999);
    map.setCenter(initialLocation);
    marker = new google.maps.Marker({
      position: initialLocation,
      map: map,
      draggable:true,
      title:"Location!"
    });
    google.maps.event.addListener(marker, 'dragend', function(evt){
      document.getElementById("txtLad").value = evt.latLng.lat();
      document.getElementById("txtLon").value = evt.latLng.lng();
    });
    document.getElementById("txtLad").value = 13.7122499;
    document.getElementById("txtLon").value = 100.59655819999999;
  }
}

function setOnChangeAssetType(){
  $('#ddlAssetType').change(onChangeAssetType);
}
function onChangeAssetType(){
  var assetTypeId = $('#ddlAssetType').val();
  displayOnChangeAssetType(assetTypeId);
}
function displayOnChangeAssetType(assetTypeId){
  assetTypeId = parseInt(assetTypeId);
  if(assetTypeId == 5){
    $('.not-land').hide();
  }else{
    $('.not-land').show();
  }
  if([1,2,3,4,5].indexOf(assetTypeId) >= 0){
    console.log('not commercial');
    $('.not-commercial').show();
    $('.option-group-1').show();
    $('.option-group-2').hide();
  }else{
    console.log('commercial');
    $('.not-commercial').hide();
    $('.option-group-1').hide();
    $('.option-group-2').show();
  }
  if([3,7,8].indexOf(assetTypeId) >= 0){
    $('.land-area').hide();
  }else{
    $('.land-area').show();
  }
}

function setOnChangeProvince(){
  $('#ddlProvince').change(function(){
    var provinceId = $(this).val();
    getAmphurList(provinceId);
  });
}
function setOnChangeAmphur(){
  $('#ddlAmphur').change(function(){
    var amphurId = $(this).val();
    getDistrictList(amphurId);
  });
}
function getAmphurList(provinceId){
  $("#ddlAmphur").empty().append('<option>Loading ...</option>');
  $("#ddlDistrict").empty().append('<option value="0">โปรดระบุ</option>');
  var posting = $.post(BASE_URL+'admin.php/thailand/amphur_list/'+provinceId);
  posting.done(function(data) {
    $("#ddlAmphur").empty();
    setAmphurOption(data);
  });
  posting.fail(function(data){
    setTimeout(function() { getAmphurList(provinceId); }, 1000);
  });
}
function setAmphurOption(data){
  $("#ddlAmphur").empty()
  $("#ddlAmphur").append('<option value="0">โปรดระบุ</option>');
  $("#ddlDistrict").empty().append('<option value="0">โปรดระบุ</option>');
  $.each( data.rows , function( key, val ) {
    console.log(val.amphur_id, val.amphur_name);
    $("#ddlAmphur").append('<option value="'+val.amphur_id+'">'+val.amphur_name+'</option>');
  });
}
function getDistrictList(amphurId){
  $("#ddlDistrict").empty().append('<option>Loading ...</option>');
  var posting = $.post(BASE_URL+'admin.php/thailand/district_list/'+amphurId);
  posting.done(function(data) {
    $("#ddlDistrict").empty();
    setDistrictOption(data);
  });
  posting.fail(function(data){
    setTimeout(function() { getDistrictList(amphurId); }, 1000);
  });
}
function setDistrictOption(data){
  $("#ddlDistrict").empty();
  $("#ddlDistrict").append('<option value="0">โปรดระบุ</option>');
  $.each( data.rows , function( key, val ) {
    console.log(val.district_id, val.district_name);
    $("#ddlDistrict").append('<option value="'+val.district_id+'">'+val.district_name+'</option>');
  });
}
