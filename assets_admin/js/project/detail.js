var currentUrl = document.URL;
var BASE_URL = currentUrl.substring(0, currentUrl.indexOf("admin.php")+10);
var updateImagePriorityUrl = BASE_URL +'announcements/updateImagePriority/';
$(document).ready(function() {
  getContentImages();
  addEventOnModalClose();
  initialize();
});
function addSortEvent(){
  $('.sortable').sortable({
    cursor: "move",
    containment: 'div.sortable',
    start: function(e, ui) {
      ui.placeholder.height(ui.item.height());
    },
    stop: function(e, ui) {
      $announcementImageId = ui.item.data('announcementimageid');
      $priority = (parseInt($('div[data-announcementimageid="' + $announcementImageId + '"]', $(this)).index()) + 1);
      $.post(updateImagePriorityUrl, {announcementImageId:$announcementImageId, sortPriority: $priority});
    }
  }).disableSelection();
}
function getContentImages(){
  $("#resultListImages").empty().append('Loading ...');
  var action = $("#resultListImages").data('action');
  var posting = $.post(action);
  posting.done(function(data) {
    $("#resultListImages").empty().append(data);
    addSortEvent();
  });
  posting.fail(function(data){
    setTimeout(function() { getContentImages(); }, 5000);
  });
}
function addEventOnModalClose(){
    $('#addImageModal').on('hide.bs.modal', function (e) {
        getContentImages();
    });
}

function initialize() {
  var mapOptions = {
    zoom: 15
  };
  var map = new google.maps.Map(document.getElementById('map-canvas'),
    mapOptions);
  var lad = document.getElementById("txtLad").value;
  var lon = document.getElementById("txtLon").value;
  initialLocation = new google.maps.LatLng(lad,lon);
  map.setCenter(initialLocation);
  var marker = new google.maps.Marker({
    position: initialLocation,
    map: map,
    title:"Here"
  });
}
