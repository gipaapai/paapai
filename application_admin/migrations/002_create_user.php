<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_user extends CI_Migration {
  public function up(){
    $this->create_user_table();
    $this->add_user();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_user');
  }
  private function create_user_table(){
    $this->dbforge->add_field(array(
      'user_id' => array(
        'type' => 'BIGINT',
        'constraint' => 20,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'user_email' => array(
        'type' => 'CHAR',
        'constraint' => '50',
      ),
      'user_password' => array(
        'type' => 'CHAR',
        'constraint' => '50',
      ),
      'user_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show'
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'last_login_date' => array(
        'type' => 'DATETIME',
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('user_id', TRUE);
    $this->dbforge->add_key(array('user_email', 'user_password', 'is_delete'));
    $this->dbforge->create_table('tbl_user');
  }
  private function add_user(){
    $this->load->model('user/User_model');
    $user_password = $this->User_model->encrypt_password('gonggong');
    $this->db->set('user_email', 'tholop@gmail.com');
    $this->db->set('user_password', $user_password);
    $this->db->set('user_name', 'Narathip Harijiratiwong');
    $this->db->set('enable_status', 'show');
    $this->db->set('create_date', 'now()', false);
    $this->db->set('create_by', 0);
    $this->db->insert('tbl_user');
  }
}
