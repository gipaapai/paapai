<?php
class User_model extends CI_Model {
  var $login_key = 'paap@1';
  function __construct() {
    parent::__construct();
  }
  public function get_data($user_id){
    $this->db->where('user_id', $user_id);
    $this->db->where('is_delete', 'active');
    $query = $this->db->get('tbl_user');
    if($query->num_rows() > 0){
      return $query->row();
    }
    return false;
  }
  public function get_name($user_id){
    $user = $this->get_data($user_id);
    if($user){
      return $user->user_name;
    }
    return '-';
  }
  public function insert($data){
    $this->db->set('user_password', $this->encrypt_password($data['user_password']));
    $this->set_to_db($data);
    $this->db->set('create_date', 'now()', false);
    $this->db->set('create_by', $this->session->userdata('admin_id'));
    $this->db->insert('tbl_user');
    $user_id = $this->db->insert_id();
    $this->Log_action_model->insert_log('user',$user_id,'insert', 'insert new user ', $data, $this->db->last_query());
    return $user_id;
  }
  public function update($user_id, $data){
    $user = $this->get_data($user_id);
    if(!$user){
      return false;
    }
    $this->set_to_db($data);
    $this->db->set('update_date', 'now()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('user_id', $user_id);
    $this->db->update('tbl_user');
    $this->Log_action_model->insert_log('user', $user_id,'insert', 'insert new user', $data, $this->db->last_query());
  }
  public function delete($user_id){
    $this->db->set('is_delete', 'delete');
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
    $this->db->where('user_id', $user_id);
    $this->db->update('tbl_user');
    $this->Log_action_model->insert_log('user',$user_id,'insert', 'insert new user ', array(), $this->db->last_query());
  }
  private function set_to_db($data){
    $this->db->set('user_type_code', $data['user_type_code']);
    if($data['user_type_code'] == 'vip'){
      $this->db->set('vip_expire_date', $data['vip_expire_date']);
    }
    $this->db->set('user_name', $data['user_name']);
    $this->db->set('user_email', $data['user_email']);
    $this->db->set('user_phone', $data['user_phone']);
    $this->db->set('enable_status', $data['enable_status']);
    $this->db->set('update_date', 'NOW()', false);
    $this->db->set('update_by', $this->session->userdata('admin_id'));
  }
  public function encrypt_password($user_password){
      return md5($this->login_key.$user_password);
  }
}
