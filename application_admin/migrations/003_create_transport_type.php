<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Create_transport_type extends CI_Migration {
  public function up(){
    $this->create_transport_type_table();
  }
  public function down(){
    $this->dbforge->drop_table('tbl_transport_type');
  }
  private function create_transport_type_table(){
    $this->dbforge->add_field(array(
      'transport_type_id' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
        'auto_increment' => TRUE
      ),
      'transport_type_name' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'transport_icon' => array(
        'type' => 'VARCHAR',
        'constraint' => '50',
      ),
      'sort_priority' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'enable_status' => array(
        'type' => 'ENUM',
        'constraint' => array('show', 'hide'),
        'default' => 'show'
      ),
      'is_delete' => array(
        'type' => 'ENUM',
        'constraint' => array('active', 'delete'),
        'default' => 'active'
      ),
      'last_login_date' => array(
        'type' => 'DATETIME',
      ),
      'create_date' => array(
        'type' => 'DATETIME',
      ),
      'create_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
      'update_date' => array(
        'type' => 'DATETIME',
      ),
      'update_by' => array(
        'type' => 'INT',
        'constraint' => 5,
        'unsigned' => TRUE,
      ),
    ));
    $this->dbforge->add_key('transport_type_id', TRUE);
    $this->dbforge->add_key(array('is_delete'));
    $this->dbforge->create_table('tbl_transport_type');
  }
}
