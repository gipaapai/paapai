$(document).ready(function(){
  setSectionHeight();
  $(window).on('resize', setSectionHeight);
});
function setSectionHeight(){
  var sectionHeight = window.innerHeight - $('header').innerHeight() - $('footer').innerHeight();
  $('#section-home').css('height', sectionHeight);
}
